package main

import (
	"soazranking/ranking/model"

	"gorm.io/gorm"
)

func seed(db *gorm.DB) {
	var games []model.Game
	db.Find(&games)
	lol := model.Game{
		Name:     "League of Legends",
		LogoUrl:  "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
		UserElos: nil,
	}
	valo := model.Game{
		Name:     "Valorant",
		LogoUrl:  "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
		UserElos: nil,
	}
	aram := model.Game{
		Name:     "LOL ARAM",
		LogoUrl:  "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
		UserElos: nil,
	}
	codenames := model.Game{
		Name:     "Codenames",
		LogoUrl:  "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
		UserElos: nil,
	}
	rumspiel := model.Game{
		Name:     "Rumspiel-Game",
		LogoUrl:  "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
		UserElos: nil,
	}
	addIfNotExists(db, rumspiel)
	addIfNotExists(db, lol)
	addIfNotExists(db, valo)
	addIfNotExists(db, aram)
	addIfNotExists(db, codenames)

}

func addIfNotExists(db *gorm.DB, game model.Game) {
	var checkGame model.Game
	result := db.Where("name = ?", game.Name).First(&checkGame)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			db.Create(&game)
			var users []model.User
			db.Find(&users)
			for _, u := range users {
				userElo := model.UserElo{
					UserID: u.ID,
					GameID: game.ID,
					Elo:    1500,
				}
				result = db.Create(&userElo)
				if result.Error != nil {
					panic(result.Error)
				}
			}
			return
		}
		panic(result.Error)
	}
}
