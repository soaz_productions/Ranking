package main

import (
	"os"
	"soazranking/ranking/model"

	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"moul.io/zapgorm2"
)

type AppConfig struct {
	Db     string
	Logger string
}

func ProvideAppConfig() AppConfig {
	stage := os.Getenv("STAGE")
	if stage == "Production" {
		return AppConfig{
			Db:     "Postgres",
			Logger: "Production",
		}
	}
	return AppConfig{Db: "SQLite", Logger: "Production"}
}

func ProvideDb(logger *zap.SugaredLogger, conf AppConfig) *gorm.DB {
	var db *gorm.DB
	var err error
	if conf.Db == "Postgres" {
		db, err = gorm.Open(postgres.New(postgres.Config{
			DSN: os.Getenv("POSTGRES_CONNECTION"),
		}), &gorm.Config{Logger: zapgorm2.New(logger.Desugar())})
	} else {
		db, err = gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{Logger: zapgorm2.New(logger.Desugar())})
	}
	if err != nil {
		panic(err)
	}
	err = db.AutoMigrate(&model.User{}, &model.Game{}, &model.UserElo{}, &model.SubmittedGame{})
	if err != nil {
		panic(err)
	}
	seed(db)
	return db
}

func ProvideLogger(conf AppConfig) *zap.SugaredLogger {
	if conf.Logger == "Production" {
		logger, err := zap.NewProduction()
		if err != nil {
			panic(err)
		}

		return logger.Sugar()
	}
	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}

	return logger.Sugar()
}
