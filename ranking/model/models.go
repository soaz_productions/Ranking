package model

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	DiscordSnowflake string
	Name             string
	UserElos         []UserElo `gorm:"constraint:OnDelete:CASCADE"`
}
type Game struct {
	gorm.Model
	Name     string
	LogoUrl  string
	UserElos []UserElo `gorm:"constraint:OnDelete:CASCADE"`
}
type UserElo struct {
	UserID    uint `gorm:"PrimaryKey"`
	GameID    uint `gorm:"PrimaryKey"`
	Elo       int
	CreatedAt time.Time
	DeletedAt gorm.DeletedAt
	User      User
}

type SubmittedGame struct {
	gorm.Model
	SubmittingUser   User
	SubmittingUserID uint
	GameData         string
}

func (u UserElo) Rating() int {
	return u.Elo
}
