package ranking

import (
	"context"
	"database/sql"
	"errors"
	"net/http"
	"soazranking/ranking/model"
	"strings"

	discordgo "github.com/bwmarrin/discordgo"
	"github.com/gorilla/mux"
	problemdetails "github.com/mvmaasakkers/go-problemdetails"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type Server struct {
	http.Server
	db     *gorm.DB
	logger *zap.SugaredLogger
}

func NewServer(db *gorm.DB) *Server {
	router := mux.NewRouter()
	l, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	s := &Server{
		Server: http.Server{},
		db:     db,
		logger: l.Sugar(),
	}
	s.addAuthorizedRoute(router, "/me", s.GetUser, "POST", "GET")
	s.addAuthorizedRoute(router, "/users", s.GetUsers, "GET")
	s.addAuthorizedRoute(router, "/games/{gameId}/ranking", s.GetGameRanking, "GET")
	s.addAuthorizedRoute(router, "/games/{gameId}/result", s.AddResult, "POST")
	s.addAuthorizedRoute(router, "/games", s.GetGames, "GET")
	router.Methods("GET").Path("/live").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})
	router.Use(mux.CORSMethodMiddleware(router))
	s.Handler = router
	s.Addr = ":80"
	return s
}

func (s *Server) Serve() {
	s.logger.Info("Starting listening on :80")
	err := s.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func (s *Server) addAuthorizedRoute(router *mux.Router, path string, handler ServerHandler, methods ...string) {
	methods = append(methods, "OPTIONS")
	router.Methods(methods...).Path(path).HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Headers", "Authorization")
		w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		ctx := r.Context()
		s.logger.Debugw("Request",
			"path", r.URL.Path,
			"from", r.RemoteAddr)
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			problemdetails.NewHTTP(http.StatusUnauthorized).ServeJSON(w, r)
			return
		}
		if !strings.HasPrefix(authHeader, "Bearer ") {
			problemdetails.NewHTTP(http.StatusUnauthorized).ServeJSON(w, r)
			return
		}
		user, problem := s.retrieveUser(ctx, authHeader)
		if problem != nil {
			problem.ServeJSON(w, r)
			return
		}
		authorizedContext := AuthorizedRequestContext{user: user}
		handler(ctx, r, w, authorizedContext)
	})
}

type AuthorizedRequestContext struct {
	user *model.User
}

type ServerHandler func(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext)

func (s *Server) retrieveUser(ctx context.Context, authHeader string) (*model.User, *problemdetails.ProblemDetails) {
	localDiscord, err := discordgo.New(authHeader)
	defer localDiscord.Close()
	if err != nil {
		s.logger.Errorw("Could not open local discord",
			"error", err)
		return nil, problemdetails.NewHTTP(http.StatusBadGateway)
	}
	discordUser, err := localDiscord.User("@me")
	if err != nil {
		s.logger.Errorw("Could not retrieve User from discord",
			"error", err)
		return nil, problemdetails.NewHTTP(http.StatusBadGateway)
	}
	var user model.User
	result := s.db.WithContext(ctx).Where("discord_snowflake = ?", discordUser.ID).First(&user)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		err = s.db.Transaction(func(tx *gorm.DB) error {
			//Create the User Object
			user = model.User{DiscordSnowflake: discordUser.ID, Name: discordUser.Username}
			result = tx.Create(&user)
			if result.Error != nil {
				s.logger.Errorf("Error inserting new User %v", zap.Error(err))
				return problemdetails.NewHTTP(http.StatusInternalServerError)
			}
			var games []model.Game
			tx.Find(&games)
			for _, game := range games {
				userElo := model.UserElo{
					UserID: user.ID,
					GameID: game.ID,
					Elo:    1500,
				}
				result = tx.Create(&userElo)
				if result.Error != nil {
					s.logger.Errorf("Error inserting new User %v", zap.Error(err))
					return problemdetails.NewHTTP(http.StatusInternalServerError)
				}
			}
			return nil
		}, &sql.TxOptions{Isolation: sql.LevelSerializable})
		if err != nil {
			if pd, ok := err.(*problemdetails.ProblemDetails); ok {
				return nil, pd
			}
			s.logger.Errorw("Database Error while running transaction to insert User", "error", err)
			return nil, problemdetails.NewHTTP(http.StatusInternalServerError)
		}
	}
	return &user, nil
}
