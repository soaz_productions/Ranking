package ranking

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"net/http"
	"soazranking/ranking/elo"
	"soazranking/ranking/model"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/mvmaasakkers/go-problemdetails"
	"gorm.io/gorm"
)

func (s *Server) GetUser(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(requestContext.user)
	return
}

func (s *Server) GetUsers(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
	var users []model.User
	s.db.Find(&users)
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(users)
}

func (s *Server) GetGames(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
	var games []model.Game
	s.db.Find(&games)
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(games)
}

func (s *Server) AddResult(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
	gameId, err := strconv.Atoi(mux.Vars(r)["gameId"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		problemdetails.New(http.StatusBadRequest, "BadRouteParam", "Invalid Parameter", "GameId should be an Integer", fmt.Sprintf("%v", gameId)).ServeJSON(w, r)
		return
	}
	var game model.Game
	if s.db.First(&game, gameId).Error != nil {
		w.WriteHeader(http.StatusNotFound)
		problemdetails.NewHTTP(http.StatusNotFound).ServeJSON(w, r)
		return
	}
	var gameResult struct {
		PlayersTeam1 []int   `json:"players_team_1"`
		PlayersTeam2 []int   `json:"players_team_2"`
		ResultTeam1  float64 `json:"result_team_1"`
		ResultTeam2  float64 `json:"result_team_2"`
	}
	reader := io.LimitReader(r.Body, 20000000)
	err = json.NewDecoder(reader).Decode(&gameResult)
	if err != nil {
		if errors.Is(err, &json.SyntaxError{}) {
			w.WriteHeader(http.StatusBadRequest)
			problemdetails.NewHTTP(http.StatusBadRequest).ServeJSON(w, r)
			return
		}
		if errors.Is(err, &json.UnmarshalTypeError{}) {
			w.WriteHeader(http.StatusUnprocessableEntity)
			problemdetails.NewHTTP(http.StatusUnprocessableEntity).ServeJSON(w, r)
			return
		}
		s.logger.Warnw("Error Decoding Request", "error", err)
		w.WriteHeader(http.StatusBadRequest)
		problemdetails.NewHTTP(http.StatusBadRequest).ServeJSON(w, r)
		return
	}
	gameDataString, err := json.Marshal(gameResult)
	if err != nil {
		panic(err)
	}
	e := s.db.Transaction(func(tx *gorm.DB) error {
		if tx.Create(&model.SubmittedGame{
			SubmittingUserID: requestContext.user.ID,
			GameData:         string(gameDataString),
		}).Error != nil {
			s.logger.Errorw("Error inserting SubmittedGame", "error", tx.Error)
			return tx.Error
		}
		var team1Players []*model.UserElo
		if tx.Joins("User").Where("game_id = ?", gameId).Find(&team1Players, gameResult.PlayersTeam1).Error != nil {
			return tx.Error
		}
		var team2Players []*model.UserElo
		if tx.Joins("User").Where("game_id = ?", gameId).Find(&team2Players, gameResult.PlayersTeam2).Error != nil {
			return tx.Error
		}
		//Calculate Elo Adjustments
		m := SoazMatch{
			TeamA:       team1Players,
			TeamB:       team2Players,
			ResultTeamA: gameResult.ResultTeam1,
			ResultTeamB: gameResult.ResultTeam2,
		}
		updatesTeam1, updatesTeam2 := elo.Play(m)
		s.logger.Infow("Calculated Elo Updates",
			"team1[0]", updatesTeam1[0],
			"team2[0]", updatesTeam2[0])

		for i := range team1Players {
			team1Players[i].Elo += int(math.Round(updatesTeam1[i]))
			tx.Save(team1Players[i])
		}
		for i := range team2Players {
			team2Players[i].Elo += int(math.Round(updatesTeam2[i]))
			tx.Save(team2Players[i])
		}
		return nil
	}, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if e != nil {
		s.logger.Errorw("Error commiting Game Result", "error", e)
		w.WriteHeader(http.StatusInternalServerError)
		problemdetails.NewHTTP(http.StatusInternalServerError).ServeJSON(w, r)
		return
	}
}

func (s *Server) GetGameRanking(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
	gameId := mux.Vars(r)["gameId"]
	gameIdNumber, err := strconv.Atoi(gameId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		problemdetails.New(http.StatusBadRequest, "BadRouteParma", "Invalid Parameter", "GameId should be an Integer", gameId).ServeJSON(w, r)
		return
	}
	var userElosForGame []model.UserElo
	s.db.Debug().Where("game_id = ?", gameIdNumber).Order("elo desc").Joins("User").Find(&userElosForGame)
	eloReturn := make([]struct {
		Elo      int    `json:"elo"`
		Username string `json:"username"`
		User_id  uint   `json:"user_id"`
	}, len(userElosForGame))
	for i := range userElosForGame {
		eloReturn[i].Elo = userElosForGame[i].Elo
		eloReturn[i].Username = userElosForGame[i].User.Name
		eloReturn[i].User_id = userElosForGame[i].UserID
	}
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(eloReturn)
}

func (s *Server) BalanceGame(ctx context.Context, r *http.Request, w http.ResponseWriter, requestContext AuthorizedRequestContext) {
}
