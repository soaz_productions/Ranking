package ranking

import (
	"soazranking/ranking/elo"
	"soazranking/ranking/model"
)

type SoazMatch struct {
	Game        *model.Game
	TeamA       []*model.UserElo
	TeamB       []*model.UserElo
	ResultTeamA float64
	ResultTeamB float64
}

func (m SoazMatch) TeamOne() []elo.Player {
	players := make([]elo.Player, len(m.TeamA))
	for i := range m.TeamA {
		players[i] = m.TeamA[i]
	}
	return players
}
func (m SoazMatch) TeamTwo() []elo.Player {
	players := make([]elo.Player, len(m.TeamB))
	for i := range m.TeamB {
		players[i] = m.TeamB[i]
	}
	return players
}
func (m SoazMatch) ResultTeamOne() float64 {
	return m.ResultTeamA
}
func (m SoazMatch) ResultTeamTwo() float64 {
	return m.ResultTeamB
}
