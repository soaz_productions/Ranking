package elo

import "math"

const (
	deviation float64 = 400
	k         uint    = 32
)

type Player interface {
	Rating() int
}

type Match interface {
	TeamOne() []Player
	TeamTwo() []Player
	ResultTeamOne() float64
	ResultTeamTwo() float64
}

//ExpectedScore returns the expected score for Player A. Expected Score of PlayerB is 1-PlayerA
func ExpectedScore(ratingA float64, ratingB float64) float64 {
	return 1 / (1 + math.Pow(10.0, (ratingB-ratingA)/deviation))
}

func ExpectedScoreMatch(m Match) float64 {
	team1 := m.TeamOne()
	team1Avg := avg(team1)
	team2 := m.TeamTwo()
	team2Avg := avg(team2)
	return ExpectedScore(team1Avg, team2Avg)
}

func Play(m Match) (adjustmentsTeam1 []float64, adjustmentsTeam2 []float64) {
	expectedScoreTeam1 := ExpectedScoreMatch(m)
	actualScoreTeam1 := m.ResultTeamOne() / (m.ResultTeamOne() + m.ResultTeamTwo())
	delta := (actualScoreTeam1 - expectedScoreTeam1) * float64(k)

	team1Adjustments := make([]float64, len(m.TeamOne()))
	team2Adjustments := make([]float64, len(m.TeamTwo()))

	for i := range team1Adjustments {
		team1Adjustments[i] = delta
	}
	for i := range team2Adjustments {
		team2Adjustments[i] = -delta
	}
	return team1Adjustments, team2Adjustments
}

func avg(ps []Player) float64 {
	var total float64 = 0
	for _, p := range ps {
		total += float64(p.Rating())
	}
	return total / float64(len(ps))
}
