import { state } from "@angular/animations";
import { Injectable } from "@angular/core";
import { Actions, concatLatestFrom, createEffect, ofType } from "@ngrx/effects";
import { createAction, createReducer, on, props, Store } from "@ngrx/store"
import { combineLatest, EMPTY, from, noop, of } from "rxjs";
import { catchError, exhaustMap, filter, map, mapTo, mergeMap, tap } from "rxjs/operators";
import { PlayerElo, RankingApiService } from "src/app/ranking-api.service";

export interface Game {
    ID: number;
    Name: string;
    LogoUrl: string;
}

export interface GameState {
    Games: Game[];
    SelectedGame: Game | null;
    RankingList: PlayerElo[]
}


const initialGamesState: GameState = {
    Games: [],
    SelectedGame: null,
    RankingList: []
}

export const startLoadGames = createAction('[Games] Start Load Games');
export const setGames = createAction('[Games] Set Games', props<{games: Game[]}>());
export const setSelectedGame = createAction('[Games] Set Selected Game', props<{game: Game | null}>());
export const startLoadRanking = createAction('[Games] Start Load Ranking')
export const setRanking = createAction('[Games] Set Ranking', props<{rankings: PlayerElo[]}>());
export const setSelectedGameById = createAction('[Games] Set Selected Game By Id', props<{ID: number}>())

export const bla = createAction('NOOP');

export const gamesReducer = createReducer(
    initialGamesState,
    on(setGames, (state, {games}) => ({...state, Games: games})),
    on(setSelectedGame, (state, { game } ) => ({...state, SelectedGame: game})),
    on(setRanking, (state, {rankings}) => ({...state, RankingList: rankings}))
);

@Injectable()
export class GamesEffects {
    constructor(private actions$: Actions, private api: RankingApiService, private store: Store<{games: GameState}>) {}
    loadGames$ = createEffect(() => this.actions$.pipe(
        ofType(startLoadGames),
        exhaustMap(() => this.api.getGames()),
        catchError(() => EMPTY),
        map(result => setGames({games: result}))
    ));
    loadRanking$ = createEffect(() => this.actions$.pipe(
        ofType(startLoadRanking),
        concatLatestFrom(action => this.store.select(s => s.games.SelectedGame)),
        filter(([action, selectedGame]) => selectedGame !== null),
        exhaustMap(([action, selectedGame]) => this.api.getRankingForGame(selectedGame!.ID)),
        map(ranking => setRanking({rankings: ranking}))
    ));
    selectedGameLoadRanking$ = createEffect(() => this.actions$.pipe(
        ofType(setSelectedGame),
        mapTo(startLoadRanking())
    ));
    setSelectedGameById$ = createEffect(() => this.actions$.pipe(
        ofType(setSelectedGameById),
        mergeMap((action) => { return combineLatest([this.api.getGames(), of(action)])}),
        mergeMap(([games, action]) => {
            const selectedGame = games.find(g => g.ID === action.ID) ?? null;
            return from([setGames({games}), setSelectedGame({game: selectedGame})]);
        })
    ));
    logEverything = createEffect(() => this.actions$.pipe(
        tap(action => console.log(action.type)),
        filter(() => false)
    ));
}