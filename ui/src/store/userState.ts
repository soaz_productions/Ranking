import { createAction, createReducer, on, props } from "@ngrx/store"

const initialUserState = {
    Name: '',
    LoggedIn: false
}

export const setUser = createAction('[User] Set User', props<{name: string}>())

export const userReducer = createReducer(
    initialUserState,
    on(setUser, (state, {name}) => ({Name: name, LoggedIn: true}))
)

