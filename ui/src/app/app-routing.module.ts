import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesSelectorComponent } from './ranking/games-selector/games-selector.component';
import { RankingListComponent } from './ranking/ranking-list/ranking-list.component';

const routes: Routes = [{
    path: 'ranking',
    component: RankingListComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
