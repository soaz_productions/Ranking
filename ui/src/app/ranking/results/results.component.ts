import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PlayerElo, RankingApiService } from 'src/app/ranking-api.service';
import { Game, GameState, startLoadRanking } from 'src/store/gamesState';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent implements OnInit {

  leftSide: Map<number, PlayerElo> = new Map();
  rightSide: Map<number, PlayerElo> = new Map();
  leftHovered = false;
  rightHovered = false;

  winner = "1";
  leftSideWinner = true;
  rightSideWinner = false;
  selectedGame: Game | null = null;
  unsub$ = new Subject<void>();
  get leftSideEntries() {
    return [...this.leftSide.entries()];
  }

  get rightSideEntries() {
    return [...this.rightSide.entries()];
  }

  constructor(private store: Store<{ games: GameState }>, private api: RankingApiService, private changeDetector: ChangeDetectorRef) {
    this.store.select(s => s.games.SelectedGame).pipe(
      takeUntil(this.unsub$)
    ).subscribe(g => this.selectedGame = g);
  }

  ngOnInit(): void {
  }

  public dropLeft(event: DragEvent) {
    const playerElo = JSON.parse(event.dataTransfer?.getData("player_elo")!) as PlayerElo;
    if (!this.rightSide.has(playerElo.user_id)) {
      this.leftSide.set(playerElo.user_id, playerElo);
    }
    this.leftHovered = false;
  }

  public dropRight(event: DragEvent) {
    const playerElo = JSON.parse(event.dataTransfer?.getData("player_elo")!) as PlayerElo;
    if (!this.leftSide.has(playerElo.user_id)) {
      this.rightSide.set(playerElo.user_id, playerElo);
    }
    this.rightHovered = false;
  }

  public rightHover(event: any) {
    console.log("right hover");
    this.rightHovered = true;
    this.allowDrop(event);
  }

  public leftHover(event: any) {
    console.log("left hover");
    this.leftHovered = true;
    this.allowDrop(event);
  }

  public allowDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }

  public clearHoverLeft(event: any) {
    this.leftHovered = false;
    console.log("clear left hover");
  }

  public clearHoverRight(event: any) {
    this.rightHovered = false;
    console.log("clear right hover");
  }

  public removePlayer(map: Map<number, PlayerElo>, playerElo: PlayerElo) {
    map.delete(playerElo.user_id);
  }

  public SubmitResult() {
    this.api.setResult(this.selectedGame!.ID,
      this.leftSideEntries.map(([seq, elo]) => elo.user_id),
      this.rightSideEntries.map(([seq, elo]) => elo.user_id),
      parseInt(this.winner) === 1 ? 1 : 0,
      parseInt(this.winner) === 2 ? 1 : 0
    ).subscribe(
      success => {
        this.store.dispatch(startLoadRanking());
        this.leftSide.clear();
        this.rightSide.clear();
        this.changeDetector.detectChanges();
      }
    )
  }
}
