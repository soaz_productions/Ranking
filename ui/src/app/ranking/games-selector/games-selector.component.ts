import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Game, GameState, startLoadGames } from 'src/store/gamesState';

@Component({
    selector: 'app-games-selector',
    templateUrl: './games-selector.component.html',
    styleUrls: ['./games-selector.component.scss']
})
export class GamesSelectorComponent implements OnInit, OnDestroy {

    private unsub$: Subject<void> = new Subject<void>();
    public games$: Observable<Game[]>;
    private games: Game[] = [];
    public defaultId = 1;
    @Output('selectedGame') public SelectedGameId = new EventEmitter<number>();
    constructor(private state: Store<{ games: GameState }>) {
        this.games$ = this.state.select(s => s.games.Games);
        this.state.dispatch(startLoadGames());
        this.games$.pipe(
            takeUntil(this.unsub$)
        ).subscribe(g => this.games = g);
    }

    ngOnInit(): void {
        this.SelectedGameId.emit(this.defaultId);
    }

    ngOnDestroy(): void {
        this.unsub$.next();
        this.unsub$.complete();
    }
    public selectionChange(event: MatSelectChange) {
        this.SelectedGameId.emit(event.value);
    }
}
