import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { MatRow } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest, concat, Observable, of, Subject } from 'rxjs';
import { catchError, filter, finalize, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { PlayerElo, RankingApiService } from 'src/app/ranking-api.service';
import { Game, GameState, setSelectedGame, setSelectedGameById, startLoadRanking } from 'src/store/gamesState';

@Component({
    selector: 'app-ranking-list',
    templateUrl: './ranking-list.component.html',
    styleUrls: ['./ranking-list.component.scss']
})
export class RankingListComponent implements OnInit {

    public displayedColumns = ["seqNo", "username", "elo"];
    public dataSource = new RankingDataSource(this.store);
    constructor(private store: Store<{games: GameState}>, private api: RankingApiService) { }

    ngOnInit(): void {
    }
    public setGame(gameId: number) {
        this.store.dispatch(setSelectedGameById({ID: gameId}));
    }
    public dragStart(event: DragEvent, row: PlayerElo) {
        event.dataTransfer?.setData("player_elo", JSON.stringify(row));
    }
}



export class RankingDataSource implements DataSource<PlayerElo> {
    private loadingSubject$ = new BehaviorSubject(false);
    public loading$ = this.loadingSubject$.asObservable();
    constructor(private store: Store<{games: GameState}>) {
    }


    connect(collectionViewer: CollectionViewer): Observable<readonly (PlayerElo & {seqNo: number})[]> {
        this.loadRanking();
        return this.store.select(g => g.games.RankingList.map((r, index) => ({...r, seqNo: index+1})));
    }
    disconnect(collectionViewer: CollectionViewer): void {
        this.loadingSubject$.complete();
    }
    public loadRanking() {
        this.store.dispatch(startLoadRanking());
    }
}