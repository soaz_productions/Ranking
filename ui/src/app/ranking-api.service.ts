import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { Game } from 'src/store/gamesState';
import { catchError, map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})

export class RankingApiService {

    constructor(private http: HttpClient) { }

    public getSelf() {
        return this.http.get<User>(environment.ApiUrl + '/me'); 
    }
    public getGames(): Observable<Game[]> {
        return this.http.get<Game[]>(environment.ApiUrl + '/games');
    }
    public getRankingForGame(gameId: number): Observable<PlayerElo[]> {
        return this.http.get<PlayerElo[]>(environment.ApiUrl + `/games/${gameId}/ranking`);
    }
    public setResult(gameId: number, playersLeft: number[], playersRight: number[], resultLeft: number, resultRight: number): Observable<boolean> {
        return this.http.post<HttpResponse<any>>(environment.ApiUrl + `/games/${gameId}/result`, {
            players_team_1: playersLeft,
            players_team_2: playersRight,
            result_team_1: resultLeft,
            result_team_2: resultRight
        }, { observe: 'response'}).pipe(
            map(response => {
                if (response.status !== 200) {
                    return false;
                }
                return true;
            }),
            catchError(() => of(false)),
        )
    }
}

export interface User {
  ID: number;
  Name: string;
}
export interface PlayerElo {
  elo: number
  username: string
  user_id: number
}