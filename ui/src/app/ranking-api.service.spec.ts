import { TestBed } from '@angular/core/testing';

import { RankingApiService } from './ranking-api.service';

describe('RankingApiService', () => {
    let service: RankingApiService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(RankingApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
