import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OAuthModule } from 'angular-oauth2-oidc';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { userReducer } from 'src/store/userState';
import { MatToolbarModule } from '@angular/material/toolbar';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { RankingListComponent } from './ranking/ranking-list/ranking-list.component';
import { GamesEffects, gamesReducer } from 'src/store/gamesState';
import { MatSelectModule } from '@angular/material/select';
import { GamesSelectorComponent } from 'src/app/ranking/games-selector/games-selector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ResultsComponent } from './ranking/results/results.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        RankingListComponent,
        GamesSelectorComponent,
        ResultsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        OAuthModule.forRoot({
            resourceServer: {
                allowedUrls: ['http://localhost:5000', "https://api.ranking.long-bui.de"],
                sendAccessToken: true,
            }
        }),
        EffectsModule.forRoot([GamesEffects]),
        StoreModule.forRoot({ user: userReducer, games: gamesReducer }),
        MatToolbarModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatRadioModule,
        MatIconModule,
        FormsModule,
        MatProgressSpinnerModule,
        StoreDevtoolsModule.instrument({ logOnly: false }),
        BrowserAnimationsModule,
        StoreRouterConnectingModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }