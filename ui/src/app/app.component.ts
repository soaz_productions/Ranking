import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs';
import { exhaustMap, filter } from 'rxjs/operators';
import { setUser } from 'src/store/userState';
import { RankingApiService } from './ranking-api.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'soazranking';
    Username$: Observable<string>

    constructor(private oauthService: OAuthService, private api: RankingApiService, private store: Store<{user: {Name: string}}>, private router: Router) {
        this.Username$ = this.store.select(s => s.user.Name);
    }

    ngOnInit(): void {
        this.oauthService.configure({
            redirectUri: window.location.origin + '/index.html',
            clientId: '923376585439084544',
            loginUrl: 'https://discord.com/api/oauth2/authorize',
            responseType: 'code',
            scope: 'identify',
            showDebugInformation: true,
            oidc: false,
            strictDiscoveryDocumentValidation: false,
            issuer: 'https://discord.com',
            tokenEndpoint: 'https://discord.com/api/oauth2/token'
        });
        this.oauthService.tryLoginCodeFlow()
            .then((_) => {
                if (!this.oauthService.hasValidAccessToken()) {
                    this.oauthService.initCodeFlow();
                } else {
                    this.api.getSelf().subscribe(user => {
                        this.store.dispatch(setUser({name: user.Name}));
                        this.router.navigateByUrl('ranking')
                    })
                }
            })
    }
}
