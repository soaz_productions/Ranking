//go:build wireinject
// +build wireinject

package main

import (
	"soazranking/ranking"

	"github.com/google/wire"
)

func InitializeServer() *ranking.Server {
	wire.Build(ProvideDb, ProvideLogger, ranking.NewServer, ProvideAppConfig)
	return &ranking.Server{}
}
